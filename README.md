# pythogram
Extreamly simple histogram plotter in python dedicated to use from command line.

### usage
```
Usage: 	pythogram.py [options] FILENAME
	pipe data | pythogram.py [OPTIONS]

Extreamly simple histogram plotter in python dedicated to use from command
line.

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -b N_BINS, --bins=N_BINS
                        N of bins (default: sqrt(dataSize))
  -e BINS_EVERY_N, --binsEveryN=BINS_EVERY_N
                        bins every N units (not working with -b)
  -t TITLE, --title=TITLE
                        histogram title
  -i N_LINES, --ignore=N_LINES
                        ignore N first rows
  -f, --first           treat first (+ N of ignored) row as a labels
  -n, --normed          histogram will be normalized
  -c, --cumulative      cumlative histogram
  -o OUTPUT_FILE_NAME, --outputFileName=OUTPUT_FILE_NAME
                        write to file instead of display

Autor: Michał Kadlof <ramidas@gmail.com>
```
### data format
```
Some comment
Another unnesseccery line

Tab	sepparated	headings
-1.993558   1.369476    -4.558302
-0.332535   1.288806    -8.309000
-0.324542   0.827369    -4.363371
1.918274    0.412564    4.782149
-0.433140   0.261528    7.941592
...
```
### example

Let say your data looks like this
```
4gqp_H	BAD	 9
4h18_A	OK!	 0 0
4hho_A	BAD	 9
4hln_A	OK!	 0 6
4hr9_A	BAD	 11
...
```
This command 
```
cat example.data | cut -f2 | pythongram.py -t 'example histogram'
```
will give you, something like this

![figure_1.png](https://bitbucket.org/repo/Ryo5qB/images/2388097407-figure_1.png)

#### Advice
It is convinient to make link to ``pythongram.py`` file in any folder that is in your ``$PATH`` under any shorter name ex. ``histogram`` or just ``hist``.

Possible steps:
```
#!bash
git clone git@bitbucket.org:ramidas/pythogram.git
cd ~/pythogram
mkdir ~/bin
echo export PATH=$PATH:~/bin >> ~/.bashrc
ln -s pythogram.py ~/bin/hist

```