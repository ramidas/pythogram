#! /usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser
import sys, os.path
import numpy as np
from matplotlib import pyplot as plt
import warnings
warnings.filterwarnings("ignore")

basename = os.path.basename((sys.argv[0]))

def usage():
	return "\ntype %s -h for help\n\n\t%s [options] FILENAME\n\tpipe data | %s [OPTIONS]" % (basename, basename, basename)


usageText = usage()
desc=u"""Extreamly simple histogram plotter in python dedicated to use from command line."""
ver=u'%prog ver. 1.0b 1 Apr 2015 (Get current version at: https://bitbucket.org/ramidas/pythogram)'
epil=u"Autor: Michał Kadlof <ramidas@gmail.com>"


def main():
	(opts, args) = parsujOpcje()
	if len(args) == 0: labels, data = getData(None, opts.ignore_first_N_lines, opts.first)
	else: labels, data = getData(args[0], opts.ignore_first_N_lines, opts.first)
	plotData(data, labels, opts.title, opts.N_Bins, opts.bins_Every_N,opts.normed,opts.cumulative,opts.output_File_Name)

def getData(fileName, ignore, first):
	if not fileName:
		try:
			rawData = [i.strip().split() for i in sys.stdin][ignore:]
		except KeyboardInterrupt:
			print usage()
			sys.exit()
	else: rawData = [i.strip().split() for i in open(fileName)][ignore:]
	if first:
		labels = rawData[0]
		del rawData[0]
	else: labels = None
	n = len(rawData)
	m = len(rawData[0])
	for i in xrange(n):
		for j in xrange(m):
			rawData[i][j] = float(rawData[i][j])
	for i in xrange(n):
		if len(rawData[i]) != m:
			print "[ERROR] Wrong dimensions in %i line" % (i) 	
			sys.exit()
	data = np.array(rawData)
	return labels,data	

def plotData(data,labels=None,title='',bins=None,every='None',normed=False,cumulative=False,outfn=None):
	n = np.shape(data)[0]
	m = np.shape(data)[1]
	if not labels: labels = ['' for i in xrange(m)]
	if (bins and every):
		print "[ERROR] You can't use option -b and -e in the same time"
		sys.exit()
	if not (bins and every): bins = n**0.5
	if every:
		left = data.min()
		right = data.max() 
		bins = np.arange(left, right, every)
	for i in xrange(m):
		plt.hist(data[:,i], alpha=0.5, label=labels[i], bins=bins,normed=normed,cumulative=cumulative)
	plt.title(unicode(title, 'UTF-8'))
	plt.grid()
	plt.legend()
	if outfn:
		plt.savefig(outfn)
		print "Your plot was written to %s" % (outfn) 
	else:
		plt.show()
	
def parsujOpcje():
	parser = OptionParser(usage=usageText, description=desc, version=ver, epilog=epil)
	parser.add_option("-b", "--bins",		   # opcja short i long style
					  dest = "N_Bins",			# nazwa zmiennej
					  default = None,		  # wartość domyślna
					  action = "store",	# [store|store_[true|false]]
					  type = "int",		   # [string|int|float] (string domyślny)
					  help=u"N of bins (default: sqrt(dataSize))")
	parser.add_option("-e", "--binsEveryN",		   # opcja short i long style
					  dest = "bins_Every_N",			# nazwa zmiennej
					  default = None,		  # wartość domyślna
					  action = "store",	# [store|store_[true|false]]
					  type = "int",		   # [string|int|float] (string domyślny)
					  help=u"bins every N units (not working with -b)")
	parser.add_option("-t", "--title",		   # opcja short i long style
					  dest = "title",			# nazwa zmiennej
					  default = '',		  # wartość domyślna
					  action = "store",	# [store|store_[true|false]]
					  type = "string",		   # [string|int|float] (string domyślny)
					  help=u"histogram title")
	parser.add_option("-i", "--ignore",		   # opcja short i long style
					  dest = "ignore_first_N_lines",			# nazwa zmiennej
					  default = 0,		  # wartość domyślna
					  action = "store",	# [store|store_[true|false]]
					  type = "int",		   # [string|int|float] (string domyślny)
					  help=u"ignore N first rows")
	parser.add_option("-f", "--first",		   # opcja short i long style
					  dest = "first",			# nazwa zmiennej
					  default = False,		  # wartość domyślna
					  action = "store_true",	# [store|store_[true|false]]
					  help=u"treat first (+ N of ignored) row as a labels")
	parser.add_option("-n", "--normed",		   # opcja short i long style
					  dest = "normed",			# nazwa zmiennej
					  default = False,		  # wartość domyślna
					  action = "store_true",	# [store|store_[true|false]]
					  help=u"histogram will be normalized")
	parser.add_option("-c", "--cumulative",		   # opcja short i long style
					  dest = "cumulative",			# nazwa zmiennej
					  default = False,		  # wartość domyślna
					  action = "store_true",	# [store|store_[true|false]]
					  help=u"cumlative histogram")
	parser.add_option("-o", "--outputFileName",		   # opcja short i long style
					  dest = "output_File_Name",			# nazwa zmiennej
					  default = None,		  # wartość domyślna
					  action = "store",	# [store|store_[true|false]]
					  type = "string",		   # [string|int|float] (string domyślny)
					  #nargs = 1,			   # liczba argumentów opcji (1 domyślne)
					  help=u"write to file instead of display")
	(opts, args) = parser.parse_args()

	if len(args) > 1:
		print "[WARNING] more than one arguments detected. All but first will be ignored."
	return opts, args
	
if __name__ == "__main__":
	main()
